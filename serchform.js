'use strict';

var caliberSelect = document.getElementById("chooseCalibre");

var calibers = ["Select", "9 mm", "5.45 mm", "7.62 mm", "12.5 mm"];

for (var i = 0; i<calibers.length; i++) {
   var options = document.createElement("OPTION");
   options.innerHTML = calibers[i];
   options.value = calibers[i];
   caliberSelect.appendChild(options)
}


var countrySelect = document.getElementById("country");

var countries = ["Select", "Russia", "USA", "Israel", "Germany"];

for (var j = 0; j<countries.length; j++) {
   var optionsCountry = document.createElement("OPTION");
   optionsCountry.innerHTML = countries[j];
   optionsCountry.value = countries[j];
   countrySelect.appendChild(optionsCountry)
}
