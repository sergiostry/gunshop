var slider  = {

    bgImages: ["1.jpg", "2.jpg", "3.jpg"],
    slide: 0,

    set: function bgSlider(image) {
        document.getElementById("body").style.backgroundImage = "url(img/mainbackgroundImg/" + image + ")";
        document.getElementById("body").style.transition = "3s, linear";

        },

    init: function () {
        this.set(this.bgImages[this.slide])
    },

    switch: function () {
        --this.slide;
        if (this.slide < 0) this.slide = this.bgImages.length - 1;
        this.set(this.bgImages[this.slide])
    }

};

    slider.init();
    setInterval(function () {
        slider.switch();
    }, 60000);
