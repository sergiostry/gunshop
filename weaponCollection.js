'use strict';

function Weapon (name, calibre, madeIn, price, href, url) {
    this.name = name;
    this.calibre = calibre;
    this.madeIn = madeIn;
    this.price = price;
    this.weaponHref = href;
    this.count = 0;
}

var arsenal = [
    new Weapon ("M-16", "7.62 mm", "USA", 1100, "#"),
    new Weapon ("AK-47", "7.62 mm", "Russia", 1400, "#"),
    new Weapon ("AK-74", "5.45 mm", "Russia", 900, "#"),
    new Weapon ("G3", "7.62 mm", "Germany", 4000, "#"),
    new Weapon ("AKSU", "5.45 mm", "Russia", 500, "#")

];



Weapon.search = function (arr) {
    cleanSearchDisplayArea();
    var parent = document.getElementById("itemInsert");
    var liItems = parent.getElementsByTagName("LI");

    function cleanSearchDisplayArea() {
        var parent = document.getElementById("itemInsert");
        var liItems = parent.getElementsByTagName("li");
        var liItemsLength = liItems.length;
        for (var a = 0; a < liItemsLength; a++) {
            parent.removeChild(liItems[0]);
        }
    }





    function getUserInput () {
        var elements = document.querySelectorAll('select');
        var dataFromUserSearch = [];
        for (var i = 0; i < elements.length; i++) {
            dataFromUserSearch.push(elements[i].value)
        }
        return dataFromUserSearch;
    }

    function createElementForSearchResult(message, weaponHref) {

        var a = document.createElement('a');
        a.setAttribute("href", weaponHref);
        a.className = "goBottom";
        var span = document.createElement("span");
        span.textContent = "Show";

        var imgDiv = document.createElement("div");
        imgDiv.className = message;

        var div = document.createElement("div");

        var close = document.createElement("div");

        var li = document.createElement('li');
        li.className = "itemSection";

        if (!message) li.classList.add("noShow");

        li.textContent = message || "Nothing to show";

        li.appendChild(close).className = "close";
        li.appendChild(a).appendChild(div).appendChild(span);
        li.appendChild(imgDiv);

        parent.appendChild(li)

    }

    for (var i = 0; i < arr.length; i++) {
        arr[i].count = 0;
        var dataFromUserSelects = getUserInput();

        for (var f = 0; f < dataFromUserSelects.length; f++) {

            if (dataFromUserSelects[f] === "Select") continue;

            for (var key in arr[i]) {

                if (arr[i][key] == dataFromUserSelects[f])  arr[i].count++;
            }
        }

        if (dataFromUserSelects.indexOf("Select") == -1 && arr[i].count != dataFromUserSelects.length) arr[i].count = 0;

        if (arr[i].count > 0) createElementForSearchResult (arr[i].name, arr[i].weaponHref)
    }
    if (!liItems.length) { createElementForSearchResult ();
        var a = document.getElementsByClassName('goBottom');
        a[0].style.display = "none";

    }

    function del () {}

    var buttons = document.querySelectorAll('li .close');
    for (var p = 0; p < buttons.length; p++) {
        var button = buttons[p];

        button.addEventListener ("click", function() {
            var el = this.parentNode;
            el.parentNode.removeChild(el);
        })
    }


};






